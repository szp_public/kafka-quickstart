package producer.fruit;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.LocalDateTime;

import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.UUID;

public class Fruit {

  @JsonProperty
  public String id;

  @JsonProperty
  public String name;

  @JsonProperty
  public String color;

  public LocalDateTime lastUpdate;

  public Fruit() {
  }

  public Fruit(String name, String color) {
    this.name = name;
    this.color = color;
    this.id = UUID.nameUUIDFromBytes(String.valueOf(Objects.hash(name, color)).getBytes(StandardCharsets.UTF_8)).toString();
    this.lastUpdate = LocalDateTime.now();
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Fruit)) return false;
    Fruit fruit = (Fruit) o;
    return name.equals(fruit.name) && color.equals(fruit.color);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, color);
  }
}
