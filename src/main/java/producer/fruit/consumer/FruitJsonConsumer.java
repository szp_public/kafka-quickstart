package producer.fruit.consumer;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import producer.fruit.Fruit;
import producer.fruit.producer.FruitJsonSchemaProducer;

import java.time.Duration;
import java.util.Arrays;
import java.util.Random;

public class FruitJsonConsumer implements FruitConsumer {

  public static void main(String... args) {

    final FruitJsonConsumer fruitJsonConsumer = new FruitJsonConsumer();
    final Consumer consumer = fruitJsonConsumer.consumer();
    Runtime.getRuntime().addShutdownHook(new Thread(consumer::close, "Shutdown-thread"));
    consumer.subscribe(Arrays.asList(fruitJsonConsumer.topic()));
    try {
      while (true) {
        ConsumerRecords<String, Fruit> records = consumer.poll(Duration.ofMillis(100));
        records.forEach(record -> {
          System.out.printf("offset = %d, key = %s, value = %s, uuid = %s \n", record.offset(), record.key(), record.value().name, record.value().id);
        });
      }
    } catch (Exception e) {
    } finally {
      consumer.close();
    }

  }

  @Override
  public String jsonValueTypeName() {
    return Fruit.class.getName();
  }

  @Override
  public String topic() {
    return FruitJsonSchemaProducer.TOPIC;
  }

  @Override
  public String groupId() {
    Random random = new Random();
    String groupId = System.getProperty("group_id", "0");
    String groupIdComposite;
    if (groupId.equals("0")) {
      groupIdComposite = "fruit-json-consumer-" + (random.nextInt(1000) + 20);
    } else {
      groupIdComposite = "fruit-json-consumer-" + groupId;
    }
    System.out.printf("using partition %s\n", groupIdComposite);
    return groupIdComposite;
  }
}
