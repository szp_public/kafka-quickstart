package producer.fruit.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import producer.fruit.Fruit;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class FruitStringJsonProducer implements FruitProducer {

  private static final String TOPIC = "fruits";

  public static void main(String... args) throws InterruptedException {

    final FruitStringJsonProducer fruitStringJsonProducer = new FruitStringJsonProducer();

    CountDownLatch latch = new CountDownLatch(fruitStringJsonProducer.fruits().size());
    final Producer<String, String> producer = fruitStringJsonProducer.instance();
    Runtime.getRuntime().addShutdownHook(new Thread(producer::close, "Shutdown-thread"));

    ObjectMapper mapper = new ObjectMapper();
    fruitStringJsonProducer.fruits().forEach(f -> {
      final Fruit fruit = (Fruit) f;
      final String json;
      try {
        json = mapper.writeValueAsString(fruit);
      } catch (JsonProcessingException e) {
        return;
      }
      ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC, fruit.id.toString(), json);
      System.out.printf("Sending '%s' fruit to topic\n", fruit.name);

      producer.send(record, (recordMetadata, e) -> {
        latch.countDown();
        if (e != null) {
          System.err.println("Error " + e.getMessage());
        } else {
          System.out.println("Sent sucessfully " + recordMetadata.toString());
        }
      });
    });
    latch.await();
    producer.close();
  }


  @Override
  public Producer<String, String> instance() {
    Properties props = new Properties();
    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
    Producer<String, String> producer = new KafkaProducer<String, String>(props);
    return producer;
  }


}
