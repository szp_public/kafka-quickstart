package producer.fruit.producer;

import io.confluent.kafka.serializers.json.KafkaJsonSchemaSerializer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import producer.fruit.Fruit;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class FruitJsonSchemaProducer implements FruitProducer<String, Fruit> {

  public static final String TOPIC = "fruits-json-schema";

  public static void main(String... args) throws InterruptedException {
    final FruitJsonSchemaProducer fruitJsonSchemaProducer = new FruitJsonSchemaProducer();
    CountDownLatch latch = new CountDownLatch(fruitJsonSchemaProducer.fruits().size());
    final Producer<String, Fruit> producer = fruitJsonSchemaProducer.instance();
    Runtime.getRuntime().addShutdownHook(new Thread(producer::close, "Shutdown-thread"));

    fruitJsonSchemaProducer.fruits().forEach(fruit -> {
      ProducerRecord<String, Fruit> record = new ProducerRecord<>(TOPIC, String.valueOf(fruit.hashCode()), fruit);
      System.out.printf("Sending '%s' fruit to topic\n", fruit.name);

      producer.send(record, (recordMetadata, e) -> {
        latch.countDown();
        if (e != null) {
          System.err.println("Error " + e.getMessage());
        } else {
          System.out.println("Sent sucessfully " + recordMetadata.toString());
        }
      });
    });
    latch.await();
    producer.close();
  }

  @Override
  public Producer<String, Fruit> instance() {
    Properties props = new Properties();
    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaJsonSchemaSerializer.class.getName());
    props.put("num.partitions", 4);
    props.put("schema.registry.url", "http://localhost:8081");
    Producer<String, Fruit> producer = new KafkaProducer<String, Fruit>(props);
    return producer;
  }
}
