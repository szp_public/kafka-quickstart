package producer.fruit.producer;

import org.apache.kafka.clients.producer.Producer;
import producer.fruit.Fruit;

import java.util.HashSet;
import java.util.Set;

public interface FruitProducer<U, T> {
  Producer<U, T> instance();

  default Set<Fruit> fruits() {
    Set<Fruit> fruits = new HashSet<>();
    fruits.add(new Fruit("Banana", "amarela"));
    fruits.add(new Fruit("Abacate", "verde"));
    fruits.add(new Fruit("Abacaxi", "amarela"));
    fruits.add(new Fruit("Embú", "verde"));
    fruits.add(new Fruit("Caju", "amarela"));
    fruits.add(new Fruit("Pera", "verde"));
    fruits.add(new Fruit("Maçã", "vermelha"));
    fruits.add(new Fruit("Siriguela", "verde-amarela"));
    fruits.add(new Fruit("Acerola", "vermelha"));
    fruits.add(new Fruit("Damasco", "bege"));
    fruits.add(new Fruit("Pêssego", "vermelha"));
    fruits.add(new Fruit("Morango", "vermelha"));
    fruits.add(new Fruit("Caqui", "vermelha"));
    fruits.add(new Fruit("Laranja", "amarela"));
    fruits.add(new Fruit("Limão", "verde"));
    fruits.add(new Fruit("Mamão", "verde-amarela"));
    return fruits;
  }
}
